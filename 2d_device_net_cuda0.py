from __future__ import print_function
import sys
import os
from os import listdir
from os.path import isfile, isdir, join
import re
import time
import numpy as np
import Image
from scipy import ndimage, misc
#import theano.sandbox.cuda
#theano.sandbox.cuda.use('gpu1')
import theano.gpuarray 
theano.gpuarray.use('cuda1') 
import theano
import theano.tensor as T
import lasagne
from sklearn.utils import shuffle
from collections import Counter
from lasagne.layers import get_output_shape

def build_cnn(filterNumStart, res, devices, input_var=None):
	
    #input
    network = lasagne.layers.InputLayer(shape=(None, 1, res,res), input_var = input_var)
    HE = lasagne.init.HeNormal('relu')
	
    #initial conv
    network = lasagne.layers.Conv2DLayer(
            network, num_filters=filterNumStart, filter_size=(7,7),
            nonlinearity=lasagne.nonlinearities.rectify,
            W=HE)
    
    #max-pooling
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))

    #convolution
    #network = lasagne.layers.Conv2DLayer(
    #        network, num_filters=filterNumStart, filter_size=(7, 7),
    #       nonlinearity=lasagne.nonlinearities.rectify)
    
    #max-pooling
    network = lasagne.layers.MaxPool2DLayer(network, pool_size=(2, 2))
    
    #convolution
    network = lasagne.layers.Conv2DLayer(
            network, num_filters=filterNumStart, filter_size=(5, 5),
            nonlinearity=lasagne.nonlinearities.rectify)

    #fully connected with 0.5 dropout
    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5),
            num_units=256,
            nonlinearity=lasagne.nonlinearities.rectify)

    #n-unit classifying layer
    network = lasagne.layers.DenseLayer(
            lasagne.layers.dropout(network, p=.5), #dp=0.5
            num_units=devices,
            nonlinearity=lasagne.nonlinearities.softmax)
    
    sh = get_output_shape(network)
    
    return network, sh
 
    
#read X an y


def load_dataset (path, res):
	X = []
	y = []
	y_train_file = []
	#carico immagini e le metto in un array numpy, un secondo array contiene le labels
	for root, dirnames, filenames in os.walk(path):
		for filename in filenames:
			if re.search("\.(jpg|jpeg|png|bmp|tiff)$", filename):
				y_train_file.append(filename) #salvo anche il nome del file anche se non serve
				y.append(os.path.split(root)[1])
				filepath = os.path.join(root, filename)
				image = ndimage.imread(filepath, mode="L")
				image_resized = misc.imresize(image, (res, res))
				X.append(image_resized)
	
	#conto numero dispositivi per poi assegnare il numer classi al classificatore softmax
	dev_num=np.shape(Counter(y).keys())[0]	
		
	X = np.array(X)
	y = np.array(y)    
	X = np.reshape(X,(X.shape[0], 1, X.shape[1],X.shape[2]))
	
	a=np.shape(X)[0]
	
	X_train = []
	y_train = []
	X_val= []
	y_val = []
	X_test = []
	y_test = []
	
	#import pdb
	#pdb.set_trace()
	X_train, X_val = X[0:int(round(a*0.7)),:,:,:], X[(int(round(a*0.7)))+1:a-1,:,:,:]
	y_train, y_val = y[0:int(round(a*0.7))], y[(int(round(a*0.7)))+1:a-1]
	
	b=np.shape(X_val)[0]
	X1=X_val
	y1=y_val
	X_val, X_test =  X1[0:int(round(b*0.95)),:,:,:], X1[(int(round(b*0.95)))+1:b-1,:,:,:]
	y_val, y_test =  y1[0:int(round(b*0.95))], y1[int(round((b*0.95)))+1:b-1]
	X_train=np.ndarray.astype(X_train,dtype='float32')
	X_val=np.ndarray.astype(X_val,dtype='float32')
	X_test=np.ndarray.astype(X_test,dtype='float32')
	y_train=np.ndarray.astype(y_train,dtype='int')
	y_val=np.ndarray.astype(y_val,dtype='int')
	y_test=np.ndarray.astype(y_test,dtype='int')
	return X_train, X_val, X_test, y_train, y_val, y_test, dev_num

def iterate_minibatches(inputs, targets, batchsize, shuffle=True):
	#print (inputs.shape, targets.shape)
	assert len(inputs) == len(targets)
	if shuffle:
		indices = np.arange(len(inputs))
		np.random.shuffle(indices)
	for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
		if shuffle:
			excerpt = indices[start_idx:start_idx + batchsize]
		else:
			excerpt = slice(start_idx, start_idx + batchsize)
		yield inputs[excerpt], targets[excerpt]

def main (num_epochs):
	
	#parametri
	path = "/home/ubuntu/CNN_DEVICE_SPHERE/training_data" #path immagini
	res = 480 #0dimensione reshape immagine
	filterNumStart = 150  #numero iniziale filtri	
	#carico immag1ini
	print("Loading data...")
	X_train, X_val, X_test, y_train, y_val, y_test, dev_num = load_dataset(path, res)
	#import pdb
	#pdb.set_trace()
	#definisco tensori immagini
	input_var = T.tensor4('inputs')
	target_var = T.ivector('targets')
	#creo  cnn
	network, sh = build_cnn(filterNumStart, res, dev_num, input_var)
	#import pdb
	#pdb.set_trace()
	sh
	
	prediction = lasagne.layers.get_output(network)
	
	loss = lasagne.objectives.categorical_crossentropy(prediction, target_var)
	loss = loss.mean()
	
	params = lasagne.layers.get_all_params(network, trainable=True)
	updates = lasagne.updates.nesterov_momentum(loss, params, learning_rate=0.0001, momentum=0.9) #si possono usare anche altre funzioni di update
	
	test_prediction = lasagne.layers.get_output(network, deterministic=True)
	test_loss = lasagne.objectives.categorical_crossentropy(test_prediction, target_var)
	test_loss = test_loss.mean()
	
	# accuratezza
	test_acc = T.mean(T.eq(T.argmax(test_prediction, axis=1), target_var), dtype=theano.config.floatX)
	
	train_fn = theano.function([input_var, target_var], loss, updates=updates, allow_input_downcast=True)
	val_fn = theano.function([input_var, target_var], [test_loss, test_acc], allow_input_downcast=True)
	print("Starting training...")
	
	for epoch in range(num_epochs):
		# per ogni epoca si passanp i batch:
		train_err = 0
		train_batches = 0
		start_time = time.time()
		for batch in iterate_minibatches(X_train, y_train, 5, shuffle=True):
			inputs, targets = batch
			train_err += train_fn(inputs, targets)
			train_batches += 1   
		val_err = 0
		val_acc = 0
		val_batches = 0
		for batch in iterate_minibatches(X_val, y_val, 5, shuffle=True):
			inputs, targets = batch
			err, acc = val_fn(inputs, targets)
			val_err += err
			val_acc += acc
			val_batches += 1
		print("Epoch {} of {} took {:.3f}s".format(epoch + 1, num_epochs, time.time() - start_time))
		print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
		print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
		print("  validation accuracy:\t\t{:.2f} %".format(val_acc / val_batches * 100))
			
			
	test_err = 0
	test_acc = 0
	test_batches = 0
	for batch in iterate_minibatches(X_test, y_test, 5, shuffle=True):
		inputs, targets = batch
		err, acc = val_fn(inputs, targets)
		test_err += err
		test_acc += acc
		test_batches += 1
	print("Final results:")
	print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
	print("  test accuracy:\t\t{:.2f} %".format(test_acc / test_batches * 100))
	
	np.savez('model.npz', *lasagne.layers.get_all_param_values(network))


main(50)


